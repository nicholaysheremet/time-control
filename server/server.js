const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());

app.set('views', path.join(__dirname, '../frontend/dist/views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, '../frontend')));
app.use(express.static(path.join(__dirname, '../frontend/dist')));

//подключение API маршрутов
require('./routes/index')(app);

module.exports = app;