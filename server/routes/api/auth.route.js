'use strict';

const User = require("../../db/index").models.User;

module.exports = router => {
  router
    .route('/auth')
    .post((req, res) => {
      const uEmail = req.body.email;
      const uPass = req.body.pass;

      return User
        .findOne({
          where: {
            email: uEmail,
            pass: uPass,
          }
        })
        .then(result => {
          if (result.id) {
            res.cookie('email', result.email)
            res
            .status(200)
            .redirect('/');
          }
          res
            .status(200)
            .redirect('/login');
        })
        .catch(err => {
          res.send(err);
        })
    });
};