'use strict';

const User = require("../../db/index").models.User;

module.exports = router => {
  router
    .route('/user')
    .get((req, res) => {
      User
        .findAll()
        .then(result => {
          res.send(result);
        })
    })
    .post((req, res) => {
      User
        .create(req.body)
        .then(result => {
          res.status(200).send(`User added. ID is ${result.id}.`)
        })
        .catch(err => {
          res.send(err);
        })
    })

  router
    .route('/user/:id')
    .get((req, res) => {
      User
        .findOne({ where: { id: req.params.id } })
        .then(result => {
          res.send(result);
        })
    })
    .patch((req, res) => {
      User
        .update(req.body, { where: { id: req.params.id } })
        .then(result => {
          res.status(200).send(`User at ID:${req.params.id} updated.`)
        })
        .catch(err => {
          res.send(err);
        })
    })
    .delete((req, res) => {
      Books
        .destroy({ where: { id: req.params.id } })
        .then(result => {
          res.status(200).send(`User at ID:${req.params.id} deleted.`)
        })
        .catch(err => {
          res.send(err);
        })
    });
};