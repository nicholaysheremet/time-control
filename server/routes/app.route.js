'use strict';

module.exports = router => {
  router
    .route('*')
    .get((req, res) => {
      const { email } = req.cookies;
      if(email == 'admin@admin.com')
      {
        res.render('admin-app', { title: 'Админка' });
      } else {
        res.redirect('/login');
      }
    });
};