'use strict';

module.exports = router => {
  router
    .route('/login')
    .get((req, res) => {
      res.render('login-app', { title: 'Вход' });
    });
};
