module.exports = [
    {
        name: 'Админ',
        sname: 'Админ',
        surname: 'Админский',
        position: '',
        codeImgUrl: '',
        email: 'admin@admin.com',
        pass: 'admin',
    },
    {
        name: 'Модерато',
        sname: 'Модератор',
        surname: 'Модераторский',
        position: '',
        codeImgUrl: '',
        email: 'mod@mod.com',
        pass: 'mod',
    },
];