'use strict';

const modelName = "GuestRole";
const tableName = "GuestRole";

module.exports = (db, Sequelize) => {
    const schema = {};

    db.models[modelName] = db.pg.define(modelName, schema, {
        freezeTableName: true,
        tableName: tableName
    });

    db.models[modelName].associate = function (models) {
        models[modelName].belongsTo(models.Guest, {
            foreignKey: 'guestId'
        });
        models[modelName].belongsTo(models.Role, {
            foreignKey: 'roleId'
        });
        models[modelName].hasMany(models.GuestJournal, {
            foreignKey: 'guestRoleId'
        });
    };

    return db;
};