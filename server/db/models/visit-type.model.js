const modelName = "VisitType";
const tableName = "VisitType";

module.exports = (db, Sequelize) => {
    const schema = {
        name: {
            type: Sequelize.STRING(256),
            allowNull: false
        },
        description: {
            type: Sequelize.STRING(2048),
            allowNull: true
        }
    };

    db.models[modelName] = db.pg.define(modelName, schema, {
        freezeTableName: true,
        tableName: tableName
    });

    db.models[modelName].associate = function (models) {
        models[modelName].hasMany(models.Journal, {
            foreignKey: 'visitTypeId'
        });
        models[modelName].hasMany(models.GuestJournal, {
            foreignKey: 'visitTypeId'
        });
    };

    return db;
};