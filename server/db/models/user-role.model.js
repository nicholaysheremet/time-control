'use strict';

const modelName = "UserRole";
const tableName = "UserRole";

module.exports = (db, Sequelize) => {
    const schema = {};

    db.models[modelName] = db.pg.define(modelName, schema, {
        freezeTableName: true,
        tableName: tableName
    });

    db.models[modelName].associate = function (models) {
        models[modelName].belongsTo(models.User, {
            foreignKey: 'userId'
        });
        models[modelName].belongsTo(models.Role, {
            foreignKey: 'roleId'
        });
        models[modelName].hasMany(models.Journal, {
            foreignKey: 'userRoleId'
        });
        models[modelName].hasMany(models.GuestJournal, {
            foreignKey: 'userRoleId'
        });
    };

    return db;
};