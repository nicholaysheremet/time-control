'use strict';

const modelName = "GuestJournal";
const tableName = "GuestJournal";

module.exports = (db, Sequelize) => {
    const schema = {
        description: {
            type: Sequelize.STRING(1024),
            allowNull: true
        },
        time: {
            type: Sequelize.DATE,
            allowNull: true
        },
    };

    db.models[modelName] = db.pg.define(modelName, schema, {
        freezeTableName: true,
        tableName: tableName
    });

    db.models[modelName].associate = function (models) {
        models[modelName].belongsTo(models.UserRole, {
            foreignKey: 'userRoleId'
        });
        models[modelName].belongsTo(models.VisitReason, {
            foreignKey: 'visitReasonId'
        });
        models[modelName].belongsTo(models.VisitType, {
            foreignKey: 'visitTypeId'
        });
        models[modelName].belongsTo(models.GuestRole, {
            foreignKey: 'guestRoleId'
        });
    };

    return db;
};