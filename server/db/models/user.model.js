'use strict';
const modelName = "User";
const tableName = "User";

module.exports = (db, Sequelize) => {
    const schema = {
        name: {
            type: Sequelize.STRING(64),
            allowNull: true
        },
        sname: {
            type: Sequelize.STRING(64),
            allowNull: true
        },
        surname: {
            type: Sequelize.STRING(128),
            allowNull: true
        },
        position: {
            type: Sequelize.STRING(512),
            allowNull: true
        },
        codeImgUrl: {
            type: Sequelize.STRING(512),
            allowNull: true
        },
        email: {
            type: Sequelize.STRING(64),
            allowNull: false
        },
        pass: {
            type: Sequelize.STRING(64),
            allowNull: false
        },
    };

    db.models[modelName] = db.pg.define(modelName, schema, {
        freezeTableName: true,
        tableName: tableName
    });

    db.models[modelName].associate = function (models) {
        models[modelName].hasMany(models.UserToken, {
            foreignKey: 'userId'
        });
        models[modelName].hasMany(models.UserRole, {
            foreignKey: 'userId'
        });
    };
    return db;
};