'use strict';

const modelName = "UserToken";
const tableName = "UserToken";

module.exports = (db, Sequelize) => {
    const schema = {
        token: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
    };

    db.models[modelName] = db.pg.define(modelName, schema, {
        freezeTableName: true,
        tableName: tableName
    });

    db.models[modelName].associate = function (models) {
        models[modelName].belongsTo(models.User, {
            foreignKey: 'userId'
        })
    };

    return db;
};