'use strict';
const modelName = "Guest";
const tableName = "Guest";

module.exports = (db, Sequelize) => {
    const schema = {
        name: {
            type: Sequelize.STRING(64),
            allowNull: true
        },
        sname: {
            type: Sequelize.STRING(64),
            allowNull: true
        },
        surname: {
            type: Sequelize.STRING(128),
            allowNull: true
        },
        position: {
            type: Sequelize.STRING(512),
            allowNull: true
        },
        institution: {
            type: Sequelize.STRING(512),
            allowNull: true
        },
    };

    db.models[modelName] = db.pg.define(modelName, schema, {
        freezeTableName: true,
        tableName: tableName
    });

    db.models[modelName].associate = function (models) {
        models[modelName].hasMany(models.GuestRole, {
            foreignKey: 'guestId'
        });
    };
    return db;
};