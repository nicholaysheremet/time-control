var adminApp = angular.module('adminApp', ['ui.router']);

adminApp.config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('home', {
      url: '/',
      template: '<h1>Вы вошли в администрационную панель.</h1>'
    })
    .state('manual', {
      url: '/manual',
      template: require('./modules/manual/manual.html'),
      controller: 'manualController',
    })
    .state('users', {
      url: '/users',
      template: require('./modules/users/users.html'),
      controller: 'usersController',
    });
}]);

require('./modules/index');
require('./admin.css')