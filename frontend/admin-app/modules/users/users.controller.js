angular.module('adminApp').controller('usersController', function ($scope) {
  let vm = $scope;
  vm.userList = [
    {
      name: 'Петр',
      sname: 'Петров',
    },
    {
      name: 'Николай',
      sname: 'Николаевич',
    },
    {
      name: 'Алексей',
      sname: 'Алексеевич',
    },
  ]
});