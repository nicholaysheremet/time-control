const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const NODE_ENV = process.env.NODE_ENV || 'development'

module.exports = {
  mode: NODE_ENV,
  entry: {
    'login-app': './frontend/login-app/index.js',
    'admin-app': './frontend/admin-app/index.js',
    'vendor': './frontend/vendor/index.js',
  },
  output: {
    filename: '[name].bundle.[chunkhash].js',
    path: path.resolve(__dirname, 'frontend', 'dist')
  },
  plugins: [
    new CleanWebpackPlugin(['frontend/dist']),
    new HtmlWebpackPlugin({
      filename: 'views/login-app.ejs', // имя создаваемого файла
      template: `!!raw-loader!${path.join(__dirname, './frontend/views/login-app.ejs')}`, // путь к шаблону
      chunks: ['vendor', 'login-app'], // блоки, которые требуется подключить в файле
      inject: 'body', // расположение собраного блока (head/body)
      chunksSortMode: 'manual', // очечердность подключаемых блоков
    }),
    new HtmlWebpackPlugin({
      filename: 'views/admin-app.ejs',
      template: `!!raw-loader!${path.join(__dirname, './frontend/views/admin-app.ejs')}`,
      chunks: ['vendor', 'admin-app'],
      inject: 'body',
      chunksSortMode: 'manual',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }
        ]
      },
      {
        test: /\.html$/,
        use: [{
          loader: 'html-loader',
        }],
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|frontend\/vendor-libs)/,
        use: [{
          loader: 'babel-loader',
        },
        ],
      },
      {
        test: /\.(scss)$/,
        use: [
          "style-loader", // creates style nodes from JS strings
          "css-loader", // translates CSS into CommonJS
          "sass-loader" // compiles Sass to CSS
        ],
      },
    ]
  }
};