'use strict'

const User = require("../server/db/index").models.User;
const UsersList = require("../server/db/datasets/test-users");
const Promise = require("bluebird");

exports.up = (next) => {
  Promise.map(UsersList, user => {
    return User.create(user);
  })
    .nodeify(next);
};

exports.down = function (next) {
  Promise.map(UsersList, user => {
    return User.destroy({
      where: {
        name: user.name
      }
    });
  })
    .nodeify(next);
};